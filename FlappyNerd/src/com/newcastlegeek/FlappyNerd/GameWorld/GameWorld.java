package com.newcastlegeek.FlappyNerd.GameWorld;

import com.newcastlegeek.FlappyNerd.GameObject.Nerd;
import com.newcastlegeek.FlappyNerd.GameObject.ScrollHandler;


public class GameWorld {
	
	private Nerd nerd;
	private ScrollHandler scroller;
	
	public GameWorld(int midPointY){
		nerd = new Nerd(33, midPointY -5, 17, 12);
		scroller = new ScrollHandler(midPointY + 66);
		
		
	}
	
	public void update(float delta){
		nerd.update(delta);
		scroller.update(delta);
				
	
	}
	
	public Nerd getNerd(){
		return nerd;
	}
    public ScrollHandler getScroller() {
        return scroller;
    }


}
