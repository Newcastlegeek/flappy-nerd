package com.newcastlegeek.FlappyNerd;

import com.badlogic.gdx.Game;
import com.newcastlegeek.FlappyNerd.FNHelpers.AssetLoader;
import com.newcastlegeek.FlappyNerd.Screens.GameScreen;

public class FlappyNerd extends Game{

	@Override
	public void create() {
		System.out.println("FlappyNerds are GO");
		AssetLoader.load();
		setScreen(new GameScreen());
		
		
		
	}
	
	public void dispose(){
		super.dispose();
		AssetLoader.dispose();
		
	}
	
	
	
	
}